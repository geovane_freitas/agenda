package com.br.agenda.resources;

import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import com.br.agenda.entity.Contato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.br.agenda.service.ContatoService;

@RestController
@RequestMapping("/resources/contatos")
public class ContatoResources {

	@Autowired
	private ContatoService contatoService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Contato>> listar() {
		return ResponseEntity.status(HttpStatus.OK).body(contatoService.listar());
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable("id") Long id) {
		Contato contato = contatoService.buscar(id);
		
		CacheControl cacheControl = CacheControl.maxAge(20, TimeUnit.SECONDS);

		return ResponseEntity.status(HttpStatus.OK).cacheControl(cacheControl).body(contato);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Contato> salvar(@Valid @RequestBody Contato contato) {
		contato = contatoService.salvar(contato);

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(contato.getId()).toUri();

		return ResponseEntity.status(HttpStatus.OK).body(contato);
	}

}

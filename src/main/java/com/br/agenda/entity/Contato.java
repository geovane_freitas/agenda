package com.br.agenda.entity;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@Entity
public class Contato implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "O campo nome não pode ser vazio")
	private String nome;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@NotEmpty(message = "O campo número não pode ser vazio")
	@Column(unique=true)
	private String numero;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
}

package com.br.agenda.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.br.agenda.entity.Contato;
import com.br.agenda.service.ContatoService;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class ContatoController {
	
	@Autowired
	private ContatoService service;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView listarContatos(@ModelAttribute("contato") Contato filtro){
		List<Contato> contatos;
		if (filtro.getNome()!=null){
			System.out.println("contato"+filtro.getNome());
			contatos = service.findByNome(filtro.getNome());
		} else {
			contatos = service.listar();
		}
		ModelAndView mv = new ModelAndView("contatos");
		mv.addObject("contatos", contatos);
		return mv;
		
	}

	@GetMapping("/add")
	public ModelAndView add(Contato contato) {
		ModelAndView mv = new ModelAndView("/detalheContato");
		mv.addObject("contato", contato);

		return mv;
	}

	@PostMapping("/save")
	public String save(@Valid Contato contato, BindingResult result) {
		//Contato contato = new Contato();

		if(result.hasErrors()) {
			if (contato.getId() != null){
				return "redirect:/"+contato.getId()+"";
			} else {
				return "redirect:/add";

			}
		}

		service.salvar(contato);

		return "redirect:/";
	}
	
	@RequestMapping("{id}")
	public ModelAndView edicao(@PathVariable("id") Contato contato) {
		ModelAndView mv = new ModelAndView("detalheContato");
		mv.addObject(contato);
		return mv;
	}

	@RequestMapping("/buscar/{nome}")
	public ModelAndView buscar(@PathVariable("nome") Contato contato) {
		System.out.println("nome " +contato.getNome());
		ModelAndView mv = new ModelAndView("contatos");
		List<Contato> contatos = service.findByNome(contato.getNome());
		mv.addObject("contatos", contatos);
		return mv;
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id) {
		 service.remover(id);
		return "redirect:/";
	}
}

package com.br.agenda.repository;

import com.br.agenda.entity.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContatoRepository extends JpaRepository<Contato, Long> {

    public List<Contato> findByNomeContainingOrderByIdAsc(String nome);

}

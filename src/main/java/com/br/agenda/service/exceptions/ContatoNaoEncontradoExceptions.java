package com.br.agenda.service.exceptions;

public class ContatoNaoEncontradoExceptions extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1869300553614629710L;

	public ContatoNaoEncontradoExceptions(String mensagem) {
		super(mensagem);
	}

	public ContatoNaoEncontradoExceptions(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}

}

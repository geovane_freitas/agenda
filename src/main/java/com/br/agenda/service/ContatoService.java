package com.br.agenda.service;

import java.util.Date;
import java.util.List;

import com.br.agenda.entity.Contato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.agenda.repository.ContatoRepository;
import com.br.agenda.service.exceptions.ContatoNaoEncontradoExceptions;

@Service
public class ContatoService {

	@Autowired
	private ContatoRepository contatoRepository;

	public List<Contato> listar() {
		return contatoRepository.findAll();
	}

	public Contato salvar(Contato contato) {
		//contato.setId(null);
		contato = contatoRepository.save(contato);
		
		return contato;
	}
	
	public Contato buscar(Long id) {
		Contato contato = contatoRepository.findOne(id);

		if (contato == null) {
			throw new ContatoNaoEncontradoExceptions("O contato não pode ser encontrado");
		}

		return contato;
	}

	public void remover(Long id){
		contatoRepository.delete(id);
	}

	public List<Contato> findByNome(String nome){
	    return contatoRepository.findByNomeContainingOrderByIdAsc(nome);
    }

}

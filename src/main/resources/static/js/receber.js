

$('.js-atualizar-status').on('click', function(event) {
		event.preventDefault();
		
		var botaoReceber = $(event.currentTarget);
		var urlReceber = botaoReceber.attr('href');
		
		var response = $.ajax({
			url: urlReceber,
			type: 'PUT'
		});
		
		
		response.done(function(e) {
			var codigoPedido = botaoReceber.data('codigo');
			$('[data-role=' + codigoPedido + ']').html('<span class="label label-success">' + e + '</span>');
			botaoReceber.hide();
		});
		
		response.fail(function(e) {
			console.log(e);
			alert('Erro em concluir o pedido');
		});
		
	});

$('.js-atualizar-status-cancelar').on('click', function(event) {
	event.preventDefault();
	
	var botaoReceber = $(event.currentTarget);
	var urlReceber = botaoReceber.attr('href');
	
	var response = $.ajax({
		url: urlReceber,
		type: 'PUT'
	});
	
	
	response.done(function(e) {
		var codigoPedido = botaoReceber.data('codigo');
		$('[data-role=' + codigoPedido + ']').html('<span class="label label-danger">' + e + '</span>');
		botaoReceber.hide();
	});
	
	response.fail(function(e) {
		console.log(e);
		alert('Erro em cancelar o pedido');
	});
	
});